const fetch = require("node-fetch");

async function fetchUser(handle) {
    const url = `https://api.github.com/users/${handle}`;
    const response = await fetch(url);
    const body = await response.json();
    if(response.status !== 200) throw Error(body.message);
     return body;
}

// fetchUser("jeantroiani")
//     .then( user => {
//         console.log(user.name);
//         console.log(user.location);
//     })
//     .catch( err => console.error(`Error: ${err.message}`)
//     )

    //or

async function showGithubUser(handle) {
    try {
        const user = await fetchUser(handle);
        console.log(user.name);
        console.log(user.location);
    } catch (err) {
        console.error(`Error: ${err.message}`);
    }
}

showGithubUser('jeantroiani');