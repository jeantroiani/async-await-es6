const fetch = require("node-fetch");

async function fetchFromGithub(endpoint) {
    const url = `https://api.github.com/${endpoint}`;
    const response = await fetch(url);
    return await response.json();

}


// async function showUserAndRepos(handle) {
//     const results = await Promise.all([
//         fetchFromGithub(`users/${handle}`),
//         fetchFromGithub(`users/${handle}/repos`)
//     ]);
//     const user = results[0];
//     const userRepos = results[1];
    
//     console.log(user.name);
//     console.log(`${userRepos.length} repos`);
// }

//or with destructoring 

async function showUserAndRepos(handle) {
    const [user, userRepos] = await Promise.all([
        fetchFromGithub(`users/${handle}`),
        fetchFromGithub(`users/${handle}/repos`)
    ]);
    
    console.log(user.name);
    console.log(`${userRepos.length} repos`);
}

showUserAndRepos('jeantroiani');