const fetch = require("node-fetch");

async function fetchFromGithub(endpoint) {
    const url = `https://api.github.com/${endpoint}`;
    const response = await fetch(url);
    return await response.json();
}

// async function showUserAndRepos(handle) {
//     const user = await fetchFromGithub(`users/${handle}`);
//     const userRepos = await fetchFromGithub(`users/${handle}/repos`);
//     console.log(user.name);
//     console.log(`${userRepos.length}`);
// }

async function showUserAndRepos(handle) {
    const userPromise =  fetchFromGithub(`users/${handle}`);
    const userReposPromise = fetchFromGithub(`users/${handle}/repos`);

    const user = await userPromise;
    const userRepos = await userReposPromise;

    console.log(user.name);
    console.log(`${userRepos.length}`);
}

showUserAndRepos('jeantroiani');