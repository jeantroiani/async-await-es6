async function main() {
    const y = await 42;
    const x = await Promise.resolve(42);
    console.log(x);
    // y = x
}

main();