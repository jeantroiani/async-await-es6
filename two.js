const fetch = require("node-fetch");

//step 1

async function showGitHubUser(handle) {
    const url = `https://api.github.com/users/${handle}`;
    const response = await fetch(url);
    return await response.json();
}

//async functions return promises.

showGitHubUser("jeantroiani")
.then(user => {
    console.log(user.name);
    console.log(user.location);
}).catch(err => console.error(err));