const fetch = require("node-fetch");

//function expresion
// const fetchGitHubUser = async handle => {
//     const url = `https://api.github.com/users/${handle}`;
//     const response = await fetch(url);
//     return await response.json();
// }



//await can only be used within a async function;
// (async () => {
//     const user = await fetchGitHubUser("jeantroiani");
//     console.log(user.name);
//     console.log(user.location);
// })();


//ES6 CLASS
class GitHubApiClient {
    async fetchUser(handle) {
        const url = `https://api.github.com/users/${handle}`;
        const response = await fetch(url);
        return await response.json();
    }
}

//Calling the class
(async () => {
    const client = new GitHubApiClient();
    const user = await client.fetchUser("jeantroiani");
    console.log(user.name);
    console.log(user.location);
})();